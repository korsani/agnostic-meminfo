# agnostic-meminfo

OS-Agnostic memory information

Tested on:

- Linux
	- raspbian
	- wsl2
	- kubernetes (via Azure containerapp)
	- Debian in Docker (via Azure webapp)
	- Mint 21.2
- Darwin (MacOS 13.6)
- FreeBSD (13.2)
- OpenBSD (7.4)

It tries to use stock info as much as possible.

## Starting

### Prequisite

- A BSD-like OS
- ```bash(1)```
- ```bc(1)```
- ```jo(1)```
- ![spod-parser.sh](https://framagit.org/korsani/spod-parser) for help displays.

### Installation

Git clone this repository.

Either copy the script in your PATH, make a symlink, or an alias.

## Using

```
	# meminfo
	Ram: 7.8G/32.0G (24%)
	Swap: 0.0G/0.0G (0%)
	Pressure: 7.8G/32.0G (24%)
```

Help can be found by:

```
	# meminfo -h
```
## Notes

On Docker, ``top(1)``` and ```free(1)``` often shows host's memory, not the memory available for the processes. ```meminfo``` returns memory available for the processes. It used cgroups, hopping it gives the right informations...

Free memory calculation is a pretty acrobatic sport when no clear tool is available (which is the case on other OS that Linux vanilla). Thus, informations shown can be unprecised, in the best case, or wrong, in the worst case. Let me know it you have better, more reliable and/more more accurate way to calculate it!

## Authors

Me :)

## License

GPL v3
