#!/usr/bin/env bash
set -u
export LC_ALL=C PS4=" \$LINENO "
HERE="$(cd "$(dirname "$0")" ; pwd)"
unit=G
declare -A UNITS
mesures=()
TERSE=""
debug=""
JSON=""
function doIMesure() {
	local m
	for m in "$@" ; do
		#grep -qE "\b$m\b"<<<"${mesures[@]}"
		# Merci OpenBSD, pfff
		grep -qE "(^| )$m($| )"<<<"${mesures[@]}" && return 0
	done
	false
}
while getopts 'dkmgrsphtj' opt ; do
	case $opt in
		d)	debug="y";;
		k)	unit="k";;
		m)	unit="M";;
		g)	unit="G";;
		r)	mesures+=("ram");;
		s)	mesures+=("swap");;
		p)	mesures+=("pressure");;
		h)	spod-parser.sh "$HERE/meminfo.spod"; exit 0;;
		t)	TERSE='y';;
		j)	JSON="y";;
	esac
done
function mlog() {
	if [ -n "$debug" ] ; then
		printf '[%s] (%3s) %s\n' "$(date)" "${BASH_LINENO[0]}" "$@" >&2
	fi
}
function IAmContainerized() {
	# Merci https://stackoverflow.com/questions/22727107/how-to-find-the-last-field-using-cut
	local guess="$(file -L --mime-type $(ps -ho cmd 1 | grep -o '[^ ]*$' ) | tail -1 | awk '{print $2}')"
	local w ; declare -a w
	if [ "$guess" = "text/x-shellscript" ] ; then	# At least it's docker
		w+=( "docker" )
	fi
	if [ -e /.dockerenv ] ; then
		w+=( "legacy" )
	fi
	if [ -n "${KUBERNETES_SERVICE_HOST:-}" ]; then
		w+=( "kub" )
	fi
	if [ -e /proc/sys/fs/binfmt_misc/WSLInterop ]  ; then
		w+=( "wsl" )
	fi
	tr ' ' '-' <<<"${w[@]}"
}
function preflighCheck() {
	local missing ; missing=()
	for bin in bc file ; do
		if ! command -v $bin >/dev/null 2>&1 ; then
			missing+=( "$bin" )
		fi
	done
	for bin in "${missing[@]}" ; do
		printf '%s(1) is missing\n' "$bin" >&2
	done
	[ ${#missing[@]} -eq 0 ]
}
# ===
preflighCheck || exit 1
if [ ${#mesures[@]} = 0 ] ; then
	mesures=( "ram" "swap" "pressure" )
fi
UNITS=( [k]=1 [M]=2 [G]=3 )
u=$((1024**${UNITS[$unit]}))
ram=0 ; physmem=1 ; swap=0 ; swapmem=1
# Memory used
case $(uname) in
	FreeBSD)
		mlog "FreeBSD"
		if doIMesure ram pressure ; then
			physmem=$(sysctl -n hw.physmem)
			ram=$(sysctl -n hw.usermem)
		fi
		if doIMesure swap pressure ; then
			swapmem="$(swapinfo | tail -1 | awk '{print $2*1024}')"
			swap="$(swapinfo | tail -1 | awk '{print $3*1024}')"
		fi
		;;
	Linux)
		mlog "Linux"
		case $(IAmContainerized) in
			docker-kub)
				mlog "flavour docker-kub"
				# A priori ca utilise le cgroup v1
				doIMesure ram pressure && physmem=$(</sys/fs/cgroup/memory.max) && ram=$(</sys/fs/cgroup/memory.current)
				doIMesure swap pressure && swapmem=$(</sys/fs/cgroup/memory.swap.max) && swap=$(</sys/fs/cgroup/memory.swap.current)
				;;
			docker-legacy)
				mlog "flavour docker-legacy"
				# La mem used renvoyée par free(1) est la mémoire totale quand on est dans un container
				# Pour peu qu'il y a cgroup v2
				doIMesure ram pressure && read -r a a physmem a < <(free -b | grep Mem) && ram=$(</sys/fs/cgroup/memory/memory.usage_in_bytes)
				# Pas de swap (jamais ?)
				doIMesure swap pressure && swapmem=0 && swap=0
				;;
			wsl)
				mlog "flavour wsl"
				doIMesure ram pressure && read -r a physmem ram b < <(free -b | grep Mem)
				doIMesure swap pressure && swapmem=0 && swap=0
				;;
			"")
				mlog "flavour vanilla"
				doIMesure ram pressure && read -r a physmem ram b < <(free -b | grep Mem)
				doIMesure swap pressure && read -r a swapmem swap b  < <(free -b | grep Swap)
				;;
		esac
		;;
	Darwin)
		mlog "Darwin"
		# total = 0.00M  used = 0.00M  free = 0.00M
		# 0.00M -> 0.00*1024^${UNITS[M]} -> 0.00*1024^2 -> (bc)
		if doIMesure swap pressure ; then
			swapmem="$(bc <<< "$(eval echo "$(sysctl -n vm.swapusage | awk '{printf $3}' | sed -e "s/\(.\)$/*1024^\${UNITS[\1]}/")")")"
			swap="$(bc <<<"$(eval echo "$(sysctl -n vm.swapusage | awk '{printf $6}' | sed -e "s/\(.\)$/*1024^\${UNITS[\1]}/")")")"
		fi
		if doIMesure ram pressure ; then
			physmem="$(sysctl -n hw.memsize)"
			# https://apple.stackexchange.com/questions/4286/is-there-a-mac-os-x-terminal-version-of-the-free-command-in-linux-systems
			# Pas très fiable, je pense
			ram=$(( physmem - $(pagesize) * $(vm_stat | grep -E "active" | awk -F":" '{f+=$2}END{print f}') ))
		fi
		;;
	OpenBSD)
		mlog "OpenBSD"
		# Memoire physique (max possible ?)
		#physmem="$(sysctl -n hw.physmem)"
		# MÃmoire user (max dispo ?)
		#physmem="$(sysctl -n hw.usermem)"
		# top(1) affiche un certain chiffre pour la ram free,
		# mais je sais pas d'où il le tient
		# ca, ca m'a l'air pas mal, d'autant que c'est a priori les mêmes info que htop(1)
		if doIMesure ram pressure ; then
			psize="$(getconf PAGESIZE)"
			physmem="$(($psize * $(systat -b uvm | grep npages | awk '{print $1}')))"
			ram="$(($psize * $(systat -b uvm | grep -E " free " | awk '{print $1}')))"
		fi
		if doIMesure swap pressure ; then
			read -r a swapmem swap a a a < <(swapctl -k | tail -1)
			((swapmem*=1024)) ; ((swap*=1024))
		fi
		;;
	*)
		printf "I'm not done for %s OS\n" "$(uname)"
		exit 2
		;;
esac
if [ $swapmem -eq 0 ] ; then
	swapmem=1
fi
# Used memory
umem="$((ram+swap))"
# Total memory
tmem="$((physmem+swapmem))"
ramoccupation="$((100*ram/physmem))"
swapoccupation="$((100*swap/swapmem))"
pressure="$((100*umem/tmem))"
if [ -n "$TERSE" ] ; then
	doIMesure ram && printf "ram: %i %i %i\n" "$ram" "$physmem" "$ramoccupation"
	doIMesure swap && printf "swap: %i %i %i\n" "$swap" "$swapmem" "$swapoccupation"
	doIMesure pressure && printf "pressure: %i %i %i\n" "$umem" "$tmem" "$pressure"
elif [ -n "$JSON" ] ; then
	joswap="" jopressure="" joram=""
	doIMesure ram && joram="ram=$(jo -- used="$ram" total="$physmem" occupation="$ramoccupation")"
	doIMesure swap && joswap="swap=$(jo -- used="$swap" total="$swapmem" occupation="$swapoccupation")"
	doIMesure pressure && jopressure="pressure=$(jo -- used="$umem" total="$tmem" occupation="$pressure")"
	jo -- $joram $joswap $jopressure
else
	doIMesure ram && printf "Ram: %.1f%s/%.1f%s (%i%%)\n" "$(bc<<<"scale=2;$ram/$u")" "$unit" "$(bc<<<"scale=2;$physmem/$u")" "$unit" "$ramoccupation"
	doIMesure swap && printf "Swap: %.1f%s/%.1f%s (%i%%)\n" "$(bc<<<"scale=2;$swap/$u")" "$unit" "$(bc<<<"scale=2;$swapmem/$u")" "$unit" "$swapoccupation"
	doIMesure pressure && printf "Pressure: %.1f%s/%.1f%s (%i%%)\n" "$(bc<<<"scale=2;$umem/$u")" "$unit" "$(bc<<<"scale=2;($tmem)/$u" )" "$unit" "$pressure"
fi
exit 0
